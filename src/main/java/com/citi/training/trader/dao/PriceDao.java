package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public interface PriceDao extends CrudRepository<Price, Integer> {

    List<Price> findByStockTicker(Stock stock);

    @Query(value = "SELECT * FROM price p JOIN stock s ON p.stock_id = s.id " +
                   "WHERE s.ticker = ?1 ORDER BY recorded_at DESC LIMIT ?2",
           nativeQuery = true)
    List<Price> findLatest(String ticker, int count);

    //int deleteOlderThan(Date cutOffTime);
}
