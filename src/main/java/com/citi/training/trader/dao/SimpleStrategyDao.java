package com.citi.training.trader.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.SimpleStrategy;

public interface SimpleStrategyDao extends CrudRepository<SimpleStrategy, Integer>{

}
