package com.citi.training.trader.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trader.model.Trade;

public interface TradeDao extends CrudRepository<Trade, Integer>{

    List<Trade> findByState(Trade.TradeState state);

    Trade findFirstByStrategyIdOrderByLastStateChangeDesc(int strategyId);

    void deleteById(int id);
}
