package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.SimpleStrategyService;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy}
 * resource.
 *
 */
@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("/api/v1/strategies")
public class SimpleStrategyController {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleStrategyController.class);

    @Autowired
    private SimpleStrategyService simpleStrategyService;

    @RequestMapping(method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SimpleStrategy> findAll() {
        LOG.info("findAll()");
        return simpleStrategyService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LOG.info("deleteById [" + id + "]");
        simpleStrategyService.deleteById(id);
    }

    @RequestMapping(method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy simpleStrategy) {
        LOG.info("HTTP POST : create[" + simpleStrategy + "]");

        SimpleStrategy createdSimpleStrategy = simpleStrategyService.save(simpleStrategy);
        LOG.info("created simpleStrategy: [" + createdSimpleStrategy + "]");

        return new ResponseEntity<SimpleStrategy>(createdSimpleStrategy, HttpStatus.CREATED);
    }
}
