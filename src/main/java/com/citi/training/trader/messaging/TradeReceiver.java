package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

/**
 * Service class that will listen on JMS queue for Trades and put
 * returned trades into the database.
 *
 */
@Component
public class TradeReceiver {

    private static final Logger LOG =
                    LoggerFactory.getLogger(TradeReceiver.class);

    @Autowired
    private TradeService tradeService;

    @JmsListener(destination = "${jms.sender.responseQueueName:OrderBroker_Reply}")
    public void receiveTrade(String xmlReply) {

        LOG.info("Processing OrderBroker Reply:");
        LOG.info(xmlReply);

        Trade replyTrade = Trade.fromXml(xmlReply);
        Trade savedTrade;
        LOG.info("Parsed returned trade: " + replyTrade);

        try {
            savedTrade = tradeService.findById(replyTrade.getId());
        } catch(EntityNotFoundException ex) {
            LOG.error("Trade reply received, but Trade not found, id: " + replyTrade.getId());
            return;
        }

        if (xmlReply.contains("<result>REJECTED</result>")) {
            savedTrade.stateChange(Trade.TradeState.REJECTED);
        } else if (xmlReply.contains("<result>Partially_Filled</result>")) {
            savedTrade.stateChange(Trade.TradeState.INIT);
        } else {
            savedTrade.stateChange(Trade.TradeState.FILLED);
        }
        tradeService.save(savedTrade);
    }
}
