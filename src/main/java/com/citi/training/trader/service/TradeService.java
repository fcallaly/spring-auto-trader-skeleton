package com.citi.training.trader.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.exceptions.EntityNotFoundException;
import com.citi.training.trader.model.Trade;
import com.google.common.collect.Lists;

@Component
public class TradeService {

    private static final Logger LOG =
                    LoggerFactory.getLogger(TradeService.class);

    @Autowired
    private TradeDao tradeDao;

    public Trade save(Trade trade) {
        return tradeDao.save(trade);
    }

    public List<Trade> findAll() {
        return Lists.newArrayList(tradeDao.findAll());
    }

    public Trade findById(int id) {
        try {
            return tradeDao.findById(id).get();
        } catch(NoSuchElementException ex) {
            String msg = "Trade not found: findById [" + id + "]";
            LOG.warn(msg);
            LOG.warn(ex.toString());
            throw new EntityNotFoundException(msg);
        }
    }

    public List<Trade> findAllByState(Trade.TradeState state) {
        return tradeDao.findByState(state);
    }

    public Trade findLatestByStrategyId(int strategyId) {
        return tradeDao.findFirstByStrategyIdOrderByLastStateChangeDesc(strategyId);
    }

    public void deleteById(int id) {
        tradeDao.deleteById(id);
    }
}
