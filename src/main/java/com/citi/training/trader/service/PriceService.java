package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@Component
public class PriceService {

    @Autowired
    private PriceDao priceDao;

    public Price save(Price price) {
        return priceDao.save(price);
    }

    public List<Price> findByStockTicker(Stock stock) {
        return priceDao.findByStockTicker(stock);
    }

    public List<Price> findLatest(Stock stock, int count) {
        return priceDao.findLatest(stock.getTicker(),  count);
    }

    //public int deleteOlderThan(Date cutOffTime) {
    //    return priceDao.deleteOlderThan(cutOffTime);
    //}
}
