package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.google.common.collect.Lists;

@Component
public class SimpleStrategyService {

    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return Lists.newArrayList(simpleStrategyDao.findAll());
    }

    public SimpleStrategy save(SimpleStrategy strategy) {
        return simpleStrategyDao.save(strategy);
    }

    public void deleteById(int id) {
        simpleStrategyDao.deleteById(id);
    }
}
