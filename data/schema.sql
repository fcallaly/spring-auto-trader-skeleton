--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticker` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;

CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `recorded_at` datetime DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5asiec6kmxfhvd4p5irtnihay` (`stock_id`),
  CONSTRAINT `FK5asiec6kmxfhvd4p5irtnihay` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `simple_strategy`
--

DROP TABLE IF EXISTS `simple_strategy`;

CREATE TABLE `simple_strategy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_position` int(11) NOT NULL,
  `exit_profit_loss` double NOT NULL,
  `last_trade_price` double NOT NULL,
  `profit` double NOT NULL,
  `size` int(11) NOT NULL,
  `stopped` datetime DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqxxthvr4vkuw8nr5aeguyen38` (`stock_id`),
  CONSTRAINT `FKqxxthvr4vkuw8nr5aeguyen38` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `trade`
--

DROP TABLE IF EXISTS `trade`;

CREATE TABLE `trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accounted_for` bit(1) NOT NULL,
  `last_state_change` datetime DEFAULT NULL,
  `price` double NOT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `trade_type` int(11) DEFAULT NULL,
  `strategy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8pmjp2f325a27rgn5rrw7ir0g` (`strategy_id`),
  CONSTRAINT `FK8pmjp2f325a27rgn5rrw7ir0g` FOREIGN KEY (`strategy_id`) REFERENCES `simple_strategy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

